package ru.nlmk.sobolevmv.tm;

import java.util.Scanner;
import static ru.nlmk.sobolevmv.tm.constant.TerminalConst.*;
import ru.nlmk.sobolevmv.tm.dao.ProjectDAO;
import ru.nlmk.sobolevmv.tm.dao.TaskDAO;
import ru.nlmk.sobolevmv.tm.entity.Project;
import ru.nlmk.sobolevmv.tm.entity.Task;

public class App {

  private static ProjectDAO projectDAO = new ProjectDAO();

  private static TaskDAO taskDAO = new TaskDAO();

  private static final Scanner scanner = new Scanner(System.in);

  static {
    projectDAO.create("DEMO PROJECT 1");
    projectDAO.create("DEMO PROJECT 2");
    projectDAO.create("DEMO PROJECT 3");
    taskDAO.create("TEST TASK 1");
    taskDAO.create("TEST TASK 2");
    taskDAO.create("TEST TASK 3");
  }

  public static void main(final String[] args) {
    run(args);
    displayWelcome();
    process();
  }

  public static void process() {
    String command = "";
    while (!CMD_EXIT.equals(command)) {
      command = scanner.nextLine();
      run(command);
      System.out.println();
    }
  }

  private static void run (final String[] args) {
    if (args == null) return;
    if (args.length < 1) return;
    final String param = args[0];
    final int  result = run(param);
    System.exit(result);
  }

  private static int run (final String param) {
    if (param == null || param.isEmpty()) return -1;
    switch (param) {
      case CMD_VERSION: return displayVersion();
      case CMD_ABOUT: return displayAbout();
      case CMD_HELP: return displayHelp ();
      case CMD_EXIT: return Exit ();

      case CMD_PROJECT_CREATE: return createProject();
      case CMD_PROJECT_CLEAR: return clearProject();
      case CMD_PROJECT_LIST: return listProject();
      case CMD_PROJECT_VIEW_BY_INDEX: return viewProjectByIndex();
      case CMD_PROJECT_VIEW_BY_ID: return viewProjectById();
      case CMD_PROJECT_REMOVE_BY_ID: return removeProjectById();
      case CMD_PROJECT_REMOVE_BY_NAME: return removeProjectByName();
      case CMD_PROJECT_REMOVE_BY_INDEX: return removeProjectByIndex();
      case CMD_PROJECT_UPDATE_BY_INDEX: return updateProjectByIndex();
      case CMD_PROJECT_UPDATE_BY_ID: return updateProjectById();

      case CMD_TASK_CREATE: return createTask();
      case CMD_TASK_CLEAR: return clearTask();
      case CMD_TASK_LIST: return listTask();
      case CMD_TASK_VIEW_BY_INDEX: return viewTaskByIndex();
      case CMD_TASK_VIEW_BY_ID: return viewTaskById();
      case CMD_TASK_REMOVE_BY_ID: return removeTaskById();
      case CMD_TASK_REMOVE_BY_NAME: return removeTaskByName();
      case CMD_TASK_REMOVE_BY_INDEX: return removeTaskByIndex();
      case CMD_TASK_UPDATE_BY_INDEX: return updateTaskByIndex();
      case CMD_TASK_UPDATE_BY_ID: return updateTaskById();
      default: return displayError();
    }
  }

  private static int createProject() {
    System.out.println("[CREATE PROJECT]");
    System.out.println("PLEASE, ENTER PROJECT NAME");
    final String name = scanner.nextLine();
    projectDAO.create(name);
    System.out.println("PLEASE, ENTER PROJECT DESCRIPTION");
    final String description = scanner.nextLine();
    projectDAO.create(name, description);
    System.out.println("[OK]");
    return 0;
  }

  private static int updateProjectByIndex() {
    System.out.println("[UPDATE PROJECT]");
    System.out.println("PLEASE, ENTER PROJECT INDEX");
    final int index = Integer.parseInt(scanner.nextLine())-1;
    final Project project = projectDAO.findByIndex(index);
    if (project == null) {
      System.out.println("[FAIL]");
      return 0;
    }
    System.out.println("PLEASE, ENTER PROJECT NAME");
    final String name = scanner.nextLine();
    System.out.println("PLEASE, ENTER PROJECT DESCRIPTION");
    final String description = scanner.nextLine();
    projectDAO.update(project.getId(), name, description);
    System.out.println("[OK]");
    return 0;
  }

  private static int updateProjectById() {
    System.out.println("[UPDATE PROJECT]");
    System.out.println("PLEASE, ENTER PROJECT ID");
    final long id = Long.parseLong(scanner.nextLine());
    final Project project = projectDAO.findById(id);
    if (project == null) {
      System.out.println("[FAIL]");
      return 0;
    }
    System.out.println("PLEASE, ENTER PROJECT NAME");
    final String name = scanner.nextLine();
    System.out.println("PLEASE, ENTER PROJECT DESCRIPTION");
    final String description = scanner.nextLine();
    projectDAO.update(project.getId(), name, description);
    System.out.println("[OK]");
    return 0;
  }

  private static int clearProject() {
    System.out.println("[CLEAR PROJECT]");
    projectDAO.clear();
    System.out.println("[OK]");
    return 0;
  }

  private static int removeProjectByName() {
    System.out.println("[REMOVE PROJECT BY NAME]");
    System.out.println("PLEASE, ENTER PROJECT NAME");
    final String name = scanner.nextLine();
    final Project project = projectDAO.removeByName(name);
    if (project == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int removeProjectById() {
    System.out.println("[REMOVE PROJECT BY ID]");
    System.out.println("PLEASE, ENTER PROJECT ID");
    final long id = scanner.nextLong();
    final Project project = projectDAO.removeById(id);
    if (project == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int removeProjectByIndex() {
    System.out.println("[REMOVE PROJECT BY INDEX]");
    System.out.println("PLEASE, ENTER PROJECT INDEX");
    final int index = scanner.nextInt() - 1;
    final Project project = projectDAO.removeByIndex(index);
    if (project == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int listProject() {
    System.out.println("[LIST PROJECT]");
    int index = 1;
    for (final Project project: projectDAO.findAll()) {
      System.out.println(index + ". " +project.getId() + ": " + project.getName());
      index++;
    }
    System.out.println("[OK]");
    return 0;
  }

  private static void viewProject(final Project project) {
    if (project == null) return;
    System.out.println("[VIEW PROJECT]");
    System.out.println("ID: " + project.getId());
    System.out.println("NAME: " + project.getName());
    System.out.println("DESCRIPTION: " + project.getDescription());
    System.out.println("[OK]");
  }

  private static int viewProjectByIndex() {
    System.out.println("ENTER, PROJECT INDEX:");
    final int index = scanner.nextInt() -1;
    final Project project = projectDAO.findByIndex(index);
    viewProject(project);
    return 0;
  }

  private static int viewProjectById() {
    System.out.println("ENTER, PROJECT ID:");
    final long id = scanner.nextLong();
    final Project project = projectDAO.findById(id);
    viewProject(project);
    return 0;
  }

  private static int createTask() {
    System.out.println("[CREATE TASK]");
    System.out.println("PLEASE, ENTER TASK NAME");
    final String name = scanner.nextLine();
    taskDAO.create(name);
    System.out.println("[OK]");
    return 0;
  }

  private static int updateTaskByIndex() {
    System.out.println("[UPDATE TASK]");
    System.out.println("PLEASE, ENTER TASK INDEX");
    final int index = Integer.parseInt(scanner.nextLine())-1;
    final Task task = taskDAO.findByIndex(index);
    if (task == null) {
      System.out.println("[FAIL]");
      return 0;
    }
    System.out.println("PLEASE, ENTER TASK NAME");
    final String name = scanner.nextLine();
    System.out.println("PLEASE, ENTER TASK DESCRIPTION");
    final String description = scanner.nextLine();
    taskDAO.update(task.getId(), name, description);
    System.out.println("[OK]");
    return 0;
  }

  private static int updateTaskById() {
    System.out.println("[UPDATE TASK]");
    System.out.println("PLEASE, ENTER TASK ID");
    final long id = Long.parseLong(scanner.nextLine());
    final Task task = taskDAO.findById(id);
    if (task == null) {
      System.out.println("[FAIL]");
      return 0;
    }
    System.out.println("PLEASE, ENTER TASK NAME");
    final String name = scanner.nextLine();
    System.out.println("PLEASE, ENTER TASK DESCRIPTION");
    final String description = scanner.nextLine();
    taskDAO.update(task.getId(), name, description);
    System.out.println("[OK]");
    return 0;
  }

  private static int clearTask() {
    System.out.println("[CLEAR TASK]");
    taskDAO.clear();
    System.out.println("[OK]");
    return 0;
  }

  private static int removeTaskByName() {
    System.out.println("[REMOVE TASK BY NAME]");
    System.out.println("PLEASE, ENTER TASK NAME");
    final String name = scanner.nextLine();
    final Task task = taskDAO.removeByName(name);
    if (task == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int removeTaskById() {
    System.out.println("[REMOVE TASK BY ID]");
    System.out.println("PLEASE, ENTER TASK ID");
    final long id = scanner.nextLong();
    final Task task = taskDAO.removeById(id);
    if (task == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int removeTaskByIndex() {
    System.out.println("[REMOVE TASK BY INDEX]");
    System.out.println("PLEASE, ENTER TASK INDEX");
    final int index = scanner.nextInt() - 1;
    final Task task = taskDAO.removeByIndex(index);
    if (task == null) System.out.println("[FAIL]");
    else System.out.println("[OK]");
    return 0;
  }

  private static int listTask() {
    System.out.println("[LIST TASK]");
    int index = 1;
    for (final Task task: taskDAO.findAll()) {
      System.out.println(index + ". " + task.getId() + ": " + task.getName());
      index++;
    }
    System.out.println("[OK]");
    return 0;
  }

  private static void viewTask(final Task task) {
    if (task == null) return;
    System.out.println("[VIEW TASK]");
    System.out.println("ID: " + task.getId());
    System.out.println("NAME: " + task.getName());
    System.out.println("DESCRIPTION: " + task.getDescription());
    System.out.println("[OK]");
  }

  private static int viewTaskByIndex() {
    System.out.println("ENTER, TASK INDEX:");
    final int index = scanner.nextInt() -1;
    final Task task = taskDAO.findByIndex(index);
    viewTask(task);
    return 0;
  }

  private static int viewTaskById() {
    System.out.println("ENTER, TASK INDEX:");
    final long id = scanner.nextLong();
    final Task task = taskDAO.findById(id);
    viewTask(task);
    return 0;
  }

  private static int displayError() {
    System.out.println("Error! Unknown program argument...");
    return -1;
  }

  private static void displayWelcome() {
    System.out.println("** WELCOME TO TASK MANAGER **");
  }

  private static int displayHelp() {
    System.out.println("version - display application version");
    System.out.println("about - display developer info");
    System.out.println("help - display list of commands");
    System.out.println("exit - terminate console application");
    System.out.println();
    System.out.println("project-create - create new project by name");
    System.out.println("project-clear - remove all project");
    System.out.println("project-list - display list of projects");
    System.out.println("project-view-by-index - display project by index");
    System.out.println("project-view-by-id - display project by id");
    System.out.println("project-remove-by-name - delete project by name");
    System.out.println("project-remove-by-id - delete project by id");
    System.out.println("project-remove-by-index - delete project by index");
    System.out.println("project-update-by-index - update project by index");
    System.out.println("project-update-by-id - update project by id");
    System.out.println();
    System.out.println("task-create - create new task by name");
    System.out.println("task-clear - remove all task");
    System.out.println("task-list - display list of tasks");
    System.out.println("task-view-by-index - display project by index");
    System.out.println("task-view-by-id - display project by id");
    System.out.println("task-remove-by-name - delete task by name");
    System.out.println("task-remove-by-id - delete task by id");
    System.out.println("task-remove-by-index - delete task by index");
    System.out.println("task-update-by-index - update task by index");
    System.out.println("task-update-by-id - update project by id");
    return 0;
  }

  private static int displayAbout() {
    System.out.println("Mikhail Sobolev");
    System.out.println("sobolev_mv@nlmk.com");
    return 0;
  }

  private static int displayVersion() {
    System.out.println("1.0.0");
    return 0;
  }

  private static int Exit() {
    System.exit(0);
    return 0;
  }

}


